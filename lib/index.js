'use strict';

var crypto = require('crypto');
var request = require('co-request');
var querystring = require('qs');
var url = require('url');
var aes = require('./aes');

/**
 * Constructor for the api object.
 *
 * @param {String} baseUrl The base URL for the SDK.
 * @param {String} accessKey The key used to associate this request with a valid entity.
 * @param {String} signatureKey The key used to generate the signature hash.
 */
var SDK = function (baseUrl, accessKey, signatureKey) {

    // Set the base URL (removing any trailing slash)
    this.baseUrl = baseUrl.replace(/\/+$/, '');

    // Set API keys
    this.accessKey = accessKey;
    this.signatureKey = signatureKey;
    this.sign = !!(accessKey && signatureKey);
}

/**
 * Disables the signature generation and X-Auth header.
 *
 * @return {Object} Returns the current object.
 */
SDK.prototype.skipSignature = function () {
    this.sign = false;
    return this;
}

/**
 * Get the URI for the specified path.
 *
 * @param {String} method The API method path.
 *
 * @return {String} Returns the full URI.
 */
SDK.prototype.getUri = function (path) {
    return this.baseUrl +'/'+ path;
}

/**
 * Create a signature using the specified timestamp and content.
 *
 * @param {Integer} timestamp An integer representing a unix timestamp.
 * @param {String} httpMethod The HTTP method of the request.
 * @param {String} path The request URI path.
 * @param {String} params The request parameters.
 *
 * @return {String} Returns the signature hash string.
 */
SDK.prototype.createSignature = function (timestamp, nonce, httpMethod, path, params) {

    var delimiter = '\n';

    // Format the signature string
    var signature = [];
    signature.push(this.accessKey);
    signature.push(timestamp);
    signature.push(nonce);
    signature.push(httpMethod.toUpperCase());
    signature.push(path || '');
    signature.push(params || '');

    // Join the signature parts using the delimiter
    signature = signature.join(delimiter);

    return crypto.createHmac('sha256', this.signatureKey).update(signature).digest('hex');
}

/**
 * Makes a HEAD request.
 *
 * @param {String} uri The URI to send the request to.
 * @param {Object} params The params to send in the request.
 * @param {Object} headers Custom headers to send with the request.
 */
SDK.prototype.head = function * (uri, params, headers) {
    return yield this.request('head', uri, params, headers);
}

/**
 * Makes a GET request.
 *
 * @param {String} uri The URI to send the request to.
 * @param {Object} params The params to send in the request.
 * @param {Object} headers Custom headers to send with the request.
 */
SDK.prototype.get = function * (uri, params, headers) {
    return yield this.request('get', uri, params, headers);
}

/**
 * Makes a POST request.
 *
 * @param {String} uri The URI to send the request to.
 * @param {Object} params The params to send in the request.
 * @param {Object} headers Custom headers to send with the request.
 */
SDK.prototype.post = function * (uri, params, headers) {
    return yield this.request('post', uri, params, headers);
}

/**
 * Makes a PUT request.
 *
 * @param {String} uri The URI to send the request to.
 * @param {Object} params The params to send in the request.
 * @param {Object} headers Custom headers to send with the request.
 */
SDK.prototype.put = function * (uri, params, headers) {
    return yield this.request('put', uri, params, headers);
}

/**
 * Makes a DELETE request.
 *
 * @param {String} uri The URI to send the request to.
 * @param {Object} params The params to send in the request.
 * @param {Object} headers Custom headers to send with the request.
 */
SDK.prototype.delete = function * (uri, params, headers) {
    return yield this.request('delete', uri, params, headers);
}

/**
 * Send a request to the SDK.
 *
 * @param {String} httpMethod The HTTP method to use (e.g. get, put).
 * @param {String} path The path to send the request to.
 * @param {Object} params The parameters to send with the request.
 * @param {Object} headers Custom headers to send with the request.
 */
SDK.prototype.request = function * (httpMethod, path, params, headers) {

    // Normalize the HTTP method
    httpMethod = httpMethod.toLowerCase();

    // Check whether this is a POST / PUT request
    var isPostPut = ([ 'post', 'put' ].indexOf(httpMethod) !== -1);

    // Process params based on the data type
    if (params) {

        // Make sure the params variable is an object
        if (typeof params !== 'object') {
            throw new Error('The params argument must be an object.');
        }

        // Encode the params
        if (isPostPut) {
            params = JSON.stringify(params);
        } else {

            // Append the params to the path
            path += '?'+ querystring.stringify(params);

            // Clear the params
            params = null;

        }

    }

    // Make sure the headers variable is an object
    if (headers && typeof headers !== 'object') {
        throw new Error('The headers argument must be an object.');
    } else if (!headers) {
        headers = {}
    }

    // Set default content type header
    if (params && !headers['content-type']) {
        if (isPostPut) {
            headers['content-type'] = 'application/json';
        } else {
            headers['content-type'] = 'application/x-www-form-urlencoded';
        }
    }

    // Get the full URI
    var uri = this.getUri(path);

    // Check if we should sign the request
    if (this.sign) {

        var urlParts    = url.parse(uri, false, true);
        var timestamp   = new Date().toISOString();
        var nonce       = crypto.randomBytes(16).toString('hex');
        var signature   = this.createSignature(
            timestamp,
            nonce,
            httpMethod,
            urlParts.path.replace(/^\//, ''),
            params
        );

        // Set the auth headers
        headers['X-Timestamp'] = timestamp;
        headers['X-Nonce'] = nonce;
        headers['X-Access-Key'] = this.accessKey;
        headers['X-Signature'] = signature;
    }

    // Set the request options
    var options = {
        json:       false,
        uri:        uri,
        method:     httpMethod,
        headers:    headers,
        body:       params
    };

    // Send the request
    var res = yield request(options);

    // Parse the data, if necessary
    if (res.body) {
        switch (true) {

            // application/json
            case /application\/json/.test(res.headers['content-type']):

                try {
                    res.body = JSON.parse(res.body);
                } catch (err) {

                    // Set the response object
                    err.res = res;

                    // Re-throw the error
                    throw err;

                }

                break;
        }
    }

    return res;
}

// Add the aes object to the SDK object so end-users can use if if necessary
SDK.aes = aes;

module.exports = SDK;