'use strict';

var crypto = require('crypto');

module.exports = {

    // Default algorithm settings
    defaultAlgorithm: 'aes-256-cbc',
    defaultFormat: 'hex',
    ivLength: 16,

    /**
     * Encrypt a plain-text string.
     *
     * @param {String} data The data to encrypt.
     * @param {String} key The key to use for the encryption.
     * @param {String} algorithm The algorithm to use for the encryption.
     * @param {String} format The output format.
     *
     * @return {String} Returns the encrypted data.
     */
    encrypt: function(data, key, algorithm, format) {

        // Get defaults if needed
        algorithm = algorithm || this.defaultAlgorithm;
        format = format || this.defaultFormat;

        // Create a random initialization vector
        var iv = crypto.randomBytes(this.ivLength);

        // Encrypt the data
        var cipher = crypto.createCipheriv(algorithm, new Buffer(key, 'hex'), iv);
        var encrypted = cipher.update(data, 'utf8', format) + cipher.final(format);

        return iv.toString('hex') + encrypted;
    },

    /**
     * Decrypt an encrypted string.
     *
     * @param {String} data The encrypted data.
     * @param {String} key The key to use for decryption.
     * @param {String} algorithm The algorithm to use for decryption.
     * @param {String} format The encrypted input format.
     *
     * @return {String} Returns the decrypted data.
     */
    decrypt: function(data, key, algorithm, format) {

        // Make sure the data is a buffer object
        if (data instanceof Buffer) {
            data = data.toString();
        }

        // Get defaults if needed
        algorithm = algorithm || this.defaultAlgorithm;
        format = format || this.defaultFormat;

        var ivLength = this.ivLength * 2;

        // Get the initialization vector
        var iv = new Buffer(data.substring(0, ivLength), 'hex');

        // Remove the iv from the data
        data = data.substring(ivLength);

        var decipher = crypto.createDecipheriv(algorithm, new Buffer(key, 'hex'), iv);
        var decrypted = decipher.update(data, format, 'utf8') + decipher.final('utf8');

        return decrypted;
    }

};